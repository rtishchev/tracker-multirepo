# скачать с подмодулем
```git clone --recursive https://gitlab.com/livestd26/tracker-multirepo.git```

# установить зависимости
```yarn```

# запуск
### вотчер. генерация типов для схемы
```yarn run gql-gen```
### запуск бфф
```yarn run start-bff```
### запуск фронта
```yarn run start-front```
import { MergeInfo } from 'graphql-tools';
import { GraphQLResolveInfo } from 'graphql';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo & { mergeInfo: MergeInfo }
) => Promise<TResult> | TResult;

export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string | number;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type CampaignColumn = {
  __typename?: 'CampaignColumn';
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type Columns = {
  __typename?: 'Columns';
  AdCampaignID?: Maybe<Scalars['String']>;
  AffNetworkID?: Maybe<Scalars['String']>;
  AffiliateNetwork?: Maybe<Scalars['String']>;
  Approve?: Maybe<Scalars['String']>;
  Bot?: Maybe<Scalars['String']>;
  Bots?: Maybe<Scalars['String']>;
  Browser?: Maybe<Scalars['String']>;
  BrowserLogo?: Maybe<Scalars['String']>;
  BrowserVersion?: Maybe<Scalars['String']>;
  CPACPL?: Maybe<Scalars['String']>;
  CPC?: Maybe<Scalars['String']>;
  CPS?: Maybe<Scalars['String']>;
  CRConversionRate?: Maybe<Scalars['String']>;
  CRHoldConversionRate?: Maybe<Scalars['String']>;
  CRSalesConversionRate?: Maybe<Scalars['String']>;
  CampID?: Maybe<Scalars['String']>;
  Campaign?: Maybe<Scalars['ID']>;
  CampaignGroup?: Maybe<Scalars['String']>;
  CampaignGroupID?: Maybe<Scalars['String']>;
  CampaignNode?: Maybe<CampaignColumn>;
  City?: Maybe<Scalars['String']>;
  Clicks?: Maybe<Scalars['String']>;
  ConnectionType?: Maybe<Scalars['String']>;
  Conversions?: Maybe<Scalars['String']>;
  Cost?: Maybe<Scalars['String']>;
  Country?: Maybe<Scalars['String']>;
  CountryFlag?: Maybe<Scalars['String']>;
  CreativeID?: Maybe<Scalars['String']>;
  DateAndTime?: Maybe<Scalars['String']>;
  Day?: Maybe<Scalars['String']>;
  DayAndHour?: Maybe<Scalars['String']>;
  Destination?: Maybe<Scalars['String']>;
  DeviceModel?: Maybe<Scalars['String']>;
  DeviceType?: Maybe<Scalars['String']>;
  ECAllEarnings?: Maybe<Scalars['String']>;
  ECConfirmed?: Maybe<Scalars['String']>;
  ECPC?: Maybe<Scalars['String']>;
  ECPMAll?: Maybe<Scalars['String']>;
  ECPMConfirmed?: Maybe<Scalars['String']>;
  EPCAll?: Maybe<Scalars['String']>;
  EPCConfirmed?: Maybe<Scalars['String']>;
  EmptyReferrers?: Maybe<Scalars['String']>;
  ExternalID?: Maybe<Scalars['String']>;
  ExtraParam1?: Maybe<Scalars['String']>;
  ExtraParam10?: Maybe<Scalars['String']>;
  ExtraParam2?: Maybe<Scalars['String']>;
  ExtraParam3?: Maybe<Scalars['String']>;
  ExtraParam4?: Maybe<Scalars['String']>;
  ExtraParam5?: Maybe<Scalars['String']>;
  ExtraParam6?: Maybe<Scalars['String']>;
  ExtraParam7?: Maybe<Scalars['String']>;
  ExtraParam8?: Maybe<Scalars['String']>;
  ExtraParam9?: Maybe<Scalars['String']>;
  Hour?: Maybe<Scalars['String']>;
  IP?: Maybe<Scalars['String']>;
  IP12?: Maybe<Scalars['String']>;
  IP123?: Maybe<Scalars['String']>;
  ISP?: Maybe<Scalars['String']>;
  Keyword?: Maybe<Scalars['String']>;
  LPCTR?: Maybe<Scalars['String']>;
  LPClickTime?: Maybe<Scalars['String']>;
  LPClicks?: Maybe<Scalars['String']>;
  LPID?: Maybe<Scalars['String']>;
  LandingPage?: Maybe<Scalars['String']>;
  Language?: Maybe<Scalars['String']>;
  Lead?: Maybe<Scalars['String']>;
  Leads?: Maybe<Scalars['String']>;
  MobileOperator?: Maybe<Scalars['String']>;
  Month?: Maybe<Scalars['String']>;
  OSLogo?: Maybe<Scalars['String']>;
  OSVersion?: Maybe<Scalars['String']>;
  Offer?: Maybe<Scalars['String']>;
  OfferID?: Maybe<Scalars['String']>;
  OperationSystem?: Maybe<Scalars['String']>;
  ParentCampaign?: Maybe<Scalars['String']>;
  ParentCampaignID?: Maybe<Scalars['String']>;
  PercentBot?: Maybe<Scalars['String']>;
  PercentUniqueClicksForCampaign?: Maybe<Scalars['String']>;
  PercentUniqueClicksForStream?: Maybe<Scalars['String']>;
  PercentUniqueClicksGlobalG?: Maybe<Scalars['String']>;
  ProfitLossAll?: Maybe<Scalars['String']>;
  ProfitLossConfirmed?: Maybe<Scalars['String']>;
  Profitability?: Maybe<Scalars['String']>;
  Proxies?: Maybe<Scalars['String']>;
  ROIAll?: Maybe<Scalars['String']>;
  ROIConfirmed?: Maybe<Scalars['String']>;
  Referrer?: Maybe<Scalars['String']>;
  Rejected?: Maybe<Scalars['String']>;
  RejectedCount?: Maybe<Scalars['String']>;
  RevenueAll?: Maybe<Scalars['String']>;
  RevenueConfirmed?: Maybe<Scalars['String']>;
  RevenueHold?: Maybe<Scalars['String']>;
  RevenueRejected?: Maybe<Scalars['String']>;
  Sale?: Maybe<Scalars['String']>;
  Sales?: Maybe<Scalars['String']>;
  SearchEngine?: Maybe<Scalars['String']>;
  Site?: Maybe<Scalars['String']>;
  Source?: Maybe<Scalars['String']>;
  StateRegion?: Maybe<Scalars['String']>;
  Stream?: Maybe<Scalars['String']>;
  StreamID?: Maybe<Scalars['String']>;
  SubID1?: Maybe<Scalars['String']>;
  SubID10?: Maybe<Scalars['String']>;
  SubID11?: Maybe<Scalars['String']>;
  SubID12?: Maybe<Scalars['String']>;
  SubID13?: Maybe<Scalars['String']>;
  SubID14?: Maybe<Scalars['String']>;
  SubID15?: Maybe<Scalars['String']>;
  SubID2?: Maybe<Scalars['String']>;
  SubID3?: Maybe<Scalars['String']>;
  SubID4?: Maybe<Scalars['String']>;
  SubID5?: Maybe<Scalars['String']>;
  SubID6?: Maybe<Scalars['String']>;
  SubID7?: Maybe<Scalars['String']>;
  SubID8?: Maybe<Scalars['String']>;
  SubID9?: Maybe<Scalars['String']>;
  SubId?: Maybe<Scalars['String']>;
  TSID?: Maybe<Scalars['String']>;
  TimeSinceLPClick?: Maybe<Scalars['String']>;
  UniqueClicksForCampaign?: Maybe<Scalars['String']>;
  UniqueClicksForStream?: Maybe<Scalars['String']>;
  UniqueClicksGlobal?: Maybe<Scalars['String']>;
  UniqueGlobal?: Maybe<Scalars['String']>;
  UniqueInStream?: Maybe<Scalars['String']>;
  UniqueInTheCampaign?: Maybe<Scalars['String']>;
  Upsells?: Maybe<Scalars['String']>;
  UseProxy?: Maybe<Scalars['String']>;
  UserAgent?: Maybe<Scalars['String']>;
  VisitorCode?: Maybe<Scalars['String']>;
  Week?: Maybe<Scalars['String']>;
  Weekday?: Maybe<Scalars['String']>;
  XRequestedWith?: Maybe<Scalars['String']>;
  Year?: Maybe<Scalars['String']>;
};

export type Inner = {
  __typename?: 'Inner';
  columns?: Maybe<Array<Columns>>;
  version?: Maybe<Scalars['String']>;
};


export type InnerColumnsArgs = {
  filters: Filters;
};

export enum OperatorList {
  Equals = 'equals',
  NotEqual = 'not_equal',
  InList = 'in_list',
  NotInList = 'not_in_list'
}

export enum OperatorNumber {
  Equals = 'equals',
  NotEqual = 'not_equal',
  GreaterThan = 'greater_than',
  LessThan = 'less_than',
  Between = 'between'
}

export enum OperatorBoolean {
  Yes = 'yes',
  No = 'no'
}

export enum OperatorString {
  Equals = 'equals',
  NotEqual = 'not_equal',
  MatchRegexp = 'match_regexp',
  NotMatchRegexp = 'not_match_regexp',
  StartsWith = 'starts_with',
  EndsWith = 'ends_with'
}

export enum OperatorStringFull {
  Contains = 'contains',
  NotContain = 'not_contain',
  Equals = 'equals',
  NotEqual = 'not_equal',
  MatchRegexp = 'match_regexp',
  NotMatchRegexp = 'not_match_regexp',
  StartsWith = 'starts_with',
  EndsWith = 'ends_with'
}

export enum OperatorLabel {
  Equals = 'equals',
  NotEqual = 'not_equal',
  StartsWith = 'starts_with',
  HasLabel = 'has_label',
  HasNotLabel = 'has_not_label'
}

export enum OperatorLabelFull {
  Contains = 'contains',
  NotContain = 'not_contain',
  Equals = 'equals',
  NotEqual = 'not_equal',
  MatchRegexp = 'match_regexp',
  NotMatchRegexp = 'not_match_regexp',
  StartsWith = 'starts_with',
  EndsWith = 'ends_with',
  HasLabel = 'has_label',
  HasNotLabel = 'has_not_label'
}

export type FilterBoolean = {
  operator: OperatorBoolean;
  value: Scalars['Boolean'];
};

export type FilterId = {
  operator: OperatorList;
  value: Scalars['ID'];
};

export type FilterNumber = {
  operator: OperatorNumber;
  value?: Maybe<Scalars['Float']>;
};

export type FilterString = {
  operator: OperatorString;
  value: Scalars['String'];
};

export type FilterStringFull = {
  operator: OperatorStringFull;
  value: Scalars['String'];
};

export type FilterLabel = {
  operator: OperatorLabel;
  value: Scalars['String'];
};

export type FilterLabelFull = {
  operator: OperatorLabelFull;
  value: Scalars['String'];
};

export type Filters = {
  campaign?: Maybe<FilterId>;
  campaign_group?: Maybe<FilterId>;
  parent_campaign?: Maybe<FilterId>;
  landing_page?: Maybe<FilterId>;
  time_since_lp_click?: Maybe<FilterNumber>;
  offer?: Maybe<FilterId>;
  affiliate_network?: Maybe<FilterId>;
  source?: Maybe<FilterId>;
  stream?: Maybe<FilterId>;
  unique_in_the_campaign?: Maybe<FilterBoolean>;
  unique_global?: Maybe<FilterBoolean>;
  site?: Maybe<FilterLabelFull>;
  x_requested_with?: Maybe<FilterLabelFull>;
  referrer?: Maybe<FilterStringFull>;
  search_engine?: Maybe<FilterStringFull>;
  keyword?: Maybe<FilterLabelFull>;
  destination?: Maybe<FilterStringFull>;
  bot?: Maybe<FilterBoolean>;
  language?: Maybe<FilterId>;
  device_type?: Maybe<FilterId>;
  user_agent?: Maybe<FilterId>;
  operation_system?: Maybe<FilterId>;
  os_version?: Maybe<FilterString>;
  browser?: Maybe<FilterId>;
  browser_version?: Maybe<FilterString>;
  device_model?: Maybe<FilterId>;
  lead?: Maybe<FilterBoolean>;
  sale?: Maybe<FilterBoolean>;
  rejected_amount?: Maybe<FilterNumber>;
  revenue_all?: Maybe<FilterNumber>;
  revenue_hold?: Maybe<FilterNumber>;
  revenue_confirmed?: Maybe<FilterNumber>;
  cost?: Maybe<FilterNumber>;
  profit_loss_all?: Maybe<FilterNumber>;
  revenue_rejected?: Maybe<FilterNumber>;
  profit_loss_confirmed?: Maybe<FilterNumber>;
  use_proxy?: Maybe<FilterBoolean>;
  country?: Maybe<FilterId>;
  state_region?: Maybe<FilterId>;
  city?: Maybe<FilterId>;
  clicks?: Maybe<FilterNumber>;
  unique_clicks_for_campaign?: Maybe<FilterNumber>;
  unique_clicks_for_stream?: Maybe<FilterNumber>;
  unique_clicks_global?: Maybe<FilterNumber>;
  p_unique_clicks_for_campaign?: Maybe<FilterNumber>;
  p_unique_clicks_for_stream?: Maybe<FilterNumber>;
  p_unique_clicks_global?: Maybe<FilterNumber>;
  bots?: Maybe<FilterNumber>;
  p_bot?: Maybe<FilterNumber>;
  proxies?: Maybe<FilterNumber>;
  empty_referrers?: Maybe<FilterNumber>;
  conversions?: Maybe<FilterNumber>;
  leads?: Maybe<FilterNumber>;
  sales?: Maybe<FilterNumber>;
  rejected_count?: Maybe<FilterNumber>;
  upsells?: Maybe<FilterNumber>;
  p_approve?: Maybe<FilterNumber>;
  lp_clicks?: Maybe<FilterNumber>;
  lp_ctr?: Maybe<FilterNumber>;
  conversion_rate?: Maybe<FilterNumber>;
  sales_conversion_rate?: Maybe<FilterNumber>;
  hold_conversion_rate?: Maybe<FilterNumber>;
  roi_all?: Maybe<FilterNumber>;
  roi_confirmed?: Maybe<FilterNumber>;
  epc_all?: Maybe<FilterNumber>;
  epc_confirmed?: Maybe<FilterNumber>;
  cps?: Maybe<FilterNumber>;
  cpa_cpl?: Maybe<FilterNumber>;
  cpc?: Maybe<FilterNumber>;
  ecpc?: Maybe<FilterNumber>;
  ecpm_all?: Maybe<FilterNumber>;
  ecpm_confirmed?: Maybe<FilterNumber>;
  ec_all?: Maybe<FilterNumber>;
  ec_confirmed?: Maybe<FilterNumber>;
  sub_id_1?: Maybe<FilterLabelFull>;
  sub_id_2?: Maybe<FilterLabelFull>;
  sub_id_3?: Maybe<FilterLabelFull>;
  sub_id_4?: Maybe<FilterLabelFull>;
  sub_id_5?: Maybe<FilterLabelFull>;
  sub_id_6?: Maybe<FilterLabelFull>;
  sub_id_7?: Maybe<FilterLabelFull>;
  sub_id_8?: Maybe<FilterLabelFull>;
  sub_id_9?: Maybe<FilterLabelFull>;
  sub_id_10?: Maybe<FilterLabelFull>;
  sub_id_11?: Maybe<FilterLabelFull>;
  sub_id_12?: Maybe<FilterLabelFull>;
  sub_id_13?: Maybe<FilterLabelFull>;
  sub_id_14?: Maybe<FilterLabelFull>;
  sub_id_15?: Maybe<FilterLabelFull>;
  extra_param_1?: Maybe<FilterStringFull>;
  extra_param_2?: Maybe<FilterStringFull>;
  extra_param_3?: Maybe<FilterStringFull>;
  extra_param_4?: Maybe<FilterStringFull>;
  extra_param_5?: Maybe<FilterStringFull>;
  extra_param_6?: Maybe<FilterStringFull>;
  extra_param_7?: Maybe<FilterStringFull>;
  extra_param_8?: Maybe<FilterStringFull>;
  extra_param_9?: Maybe<FilterStringFull>;
  extra_param_10?: Maybe<FilterStringFull>;
  sub_id?: Maybe<FilterStringFull>;
  visitor_code?: Maybe<FilterStringFull>;
  user_agent_id?: Maybe<FilterId>;
  ad_campaign_id?: Maybe<FilterLabelFull>;
  external_id?: Maybe<FilterStringFull>;
  creative_id?: Maybe<FilterLabelFull>;
  connection_type?: Maybe<FilterId>;
  mobile_operator?: Maybe<FilterId>;
  isp?: Maybe<FilterId>;
  ip?: Maybe<FilterLabel>;
  ip_1?: Maybe<FilterStringFull>;
  ip_2?: Maybe<FilterStringFull>;
};

export type Query = {
  __typename?: 'Query';
  inner?: Maybe<Inner>;
  version: Scalars['String'];
};



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> = LegacyStitchingResolver<TResult, TParent, TContext, TArgs> | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  CampaignColumn: ResolverTypeWrapper<CampaignColumn>;
  ID: ResolverTypeWrapper<Scalars['ID']>;
  String: ResolverTypeWrapper<Scalars['String']>;
  Columns: ResolverTypeWrapper<Columns>;
  Inner: ResolverTypeWrapper<Inner>;
  OperatorList: OperatorList;
  OperatorNumber: OperatorNumber;
  OperatorBoolean: OperatorBoolean;
  OperatorString: OperatorString;
  OperatorStringFull: OperatorStringFull;
  OperatorLabel: OperatorLabel;
  OperatorLabelFull: OperatorLabelFull;
  FilterBoolean: FilterBoolean;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  FilterID: FilterId;
  FilterNumber: FilterNumber;
  Float: ResolverTypeWrapper<Scalars['Float']>;
  FilterString: FilterString;
  FilterStringFull: FilterStringFull;
  FilterLabel: FilterLabel;
  FilterLabelFull: FilterLabelFull;
  Filters: Filters;
  Query: ResolverTypeWrapper<{}>;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  CampaignColumn: CampaignColumn;
  ID: Scalars['ID'];
  String: Scalars['String'];
  Columns: Columns;
  Inner: Inner;
  FilterBoolean: FilterBoolean;
  Boolean: Scalars['Boolean'];
  FilterID: FilterId;
  FilterNumber: FilterNumber;
  Float: Scalars['Float'];
  FilterString: FilterString;
  FilterStringFull: FilterStringFull;
  FilterLabel: FilterLabel;
  FilterLabelFull: FilterLabelFull;
  Filters: Filters;
  Query: {};
};

export type CampaignColumnResolvers<ContextType = any, ParentType extends ResolversParentTypes['CampaignColumn'] = ResolversParentTypes['CampaignColumn']> = {
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ColumnsResolvers<ContextType = any, ParentType extends ResolversParentTypes['Columns'] = ResolversParentTypes['Columns']> = {
  AdCampaignID?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  AffNetworkID?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  AffiliateNetwork?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Approve?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Bot?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Bots?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Browser?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  BrowserLogo?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  BrowserVersion?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  CPACPL?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  CPC?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  CPS?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  CRConversionRate?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  CRHoldConversionRate?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  CRSalesConversionRate?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  CampID?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Campaign?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>;
  CampaignGroup?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  CampaignGroupID?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  CampaignNode?: Resolver<Maybe<ResolversTypes['CampaignColumn']>, ParentType, ContextType>;
  City?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Clicks?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ConnectionType?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Conversions?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Cost?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Country?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  CountryFlag?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  CreativeID?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  DateAndTime?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Day?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  DayAndHour?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Destination?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  DeviceModel?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  DeviceType?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ECAllEarnings?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ECConfirmed?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ECPC?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ECPMAll?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ECPMConfirmed?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  EPCAll?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  EPCConfirmed?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  EmptyReferrers?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ExternalID?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ExtraParam1?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ExtraParam10?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ExtraParam2?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ExtraParam3?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ExtraParam4?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ExtraParam5?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ExtraParam6?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ExtraParam7?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ExtraParam8?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ExtraParam9?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Hour?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  IP?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  IP12?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  IP123?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ISP?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Keyword?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  LPCTR?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  LPClickTime?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  LPClicks?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  LPID?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  LandingPage?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Language?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Lead?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Leads?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  MobileOperator?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Month?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  OSLogo?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  OSVersion?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Offer?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  OfferID?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  OperationSystem?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ParentCampaign?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ParentCampaignID?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  PercentBot?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  PercentUniqueClicksForCampaign?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  PercentUniqueClicksForStream?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  PercentUniqueClicksGlobalG?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ProfitLossAll?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ProfitLossConfirmed?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Profitability?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Proxies?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ROIAll?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  ROIConfirmed?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Referrer?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Rejected?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  RejectedCount?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  RevenueAll?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  RevenueConfirmed?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  RevenueHold?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  RevenueRejected?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Sale?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Sales?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SearchEngine?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Site?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Source?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  StateRegion?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Stream?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  StreamID?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID1?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID10?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID11?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID12?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID13?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID14?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID15?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID2?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID3?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID4?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID5?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID6?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID7?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID8?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubID9?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  SubId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  TSID?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  TimeSinceLPClick?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  UniqueClicksForCampaign?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  UniqueClicksForStream?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  UniqueClicksGlobal?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  UniqueGlobal?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  UniqueInStream?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  UniqueInTheCampaign?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Upsells?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  UseProxy?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  UserAgent?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  VisitorCode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Week?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Weekday?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  XRequestedWith?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  Year?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type InnerResolvers<ContextType = any, ParentType extends ResolversParentTypes['Inner'] = ResolversParentTypes['Inner']> = {
  columns?: Resolver<Maybe<Array<ResolversTypes['Columns']>>, ParentType, ContextType, RequireFields<InnerColumnsArgs, 'filters'>>;
  version?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
  inner?: Resolver<Maybe<ResolversTypes['Inner']>, ParentType, ContextType>;
  version?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
};

export type Resolvers<ContextType = any> = {
  CampaignColumn?: CampaignColumnResolvers<ContextType>;
  Columns?: ColumnsResolvers<ContextType>;
  Inner?: InnerResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
};


/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = any> = Resolvers<ContextType>;

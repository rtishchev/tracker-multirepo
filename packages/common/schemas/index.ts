import { importSchema } from 'graphql-import';
import { mergeTypeDefs } from '@graphql-tools/merge';
import { gql } from 'apollo-server-core';
import { join } from 'path';

export const querySchema = gql(importSchema(join(__dirname, 'query.schema.graphql')));
export const innerSchema = gql(importSchema(join(__dirname, 'inner.schema.graphql')));
export const filtersSchema = gql(importSchema(join(__dirname, 'filters.schema.graphql')));
export const columnsSchema = gql(importSchema(join(__dirname, 'columns.schema.graphql')));
export const campaignSchema = gql(importSchema(join(__dirname, 'campaign.schema.graphql')));

const typeDefs = mergeTypeDefs([querySchema, innerSchema, filtersSchema, columnsSchema, campaignSchema]);

export default typeDefs;

import { ApolloServer, IResolvers } from 'apollo-server';
import typeDefs from 'common/schemas';
import { getPathFromInfo } from 'helpers/getPathFromInfo';
import { Context, CustomMethod } from 'types/Context';
import resolvers from 'resolvers';

// const typeDefs = [querySchema, nodeSchema, innerSchema, movieSchema, directorSchema];

const server = new ApolloServer({ typeDefs, resolvers: resolvers as IResolvers<any, Context>, context: async ({req}) => {
  const customMethod: CustomMethod = (info) => {
    console.log("auth method");
    const path = getPathFromInfo(info);
    console.log('get access', path);
    return true;
  };
  return {req, customMethod};
}});

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});

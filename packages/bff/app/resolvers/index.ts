import { Context } from 'types/Context';
import { Columns, Campaigns } from 'mock';
import {
  Resolvers,
  InnerResolvers,
  ColumnsResolvers,
} from 'common/types';

// field resolvers
const campaign: ColumnsResolvers<Context>['CampaignNode'] = async (source) => {
  return (source.Campaign && Campaigns.find(m => source.Campaign && m.id === +source.Campaign)) || null;
};
// inner resolvers
const columnsFn: InnerResolvers<Context>['columns'] = (source, args) => {
  return Columns;
};

// query resolvers
const resolvers: Resolvers<Context> = {
  Query: {
    inner: async (source, args, context, info) => {
      if (context.customMethod(info)) {
        return {}
      }
      // TODO if dev throw exception
      return null;
    },
  },
  Inner: {
    columns: columnsFn,
  },
  Columns: {
    CampaignNode: campaign
  }
};

export default resolvers;
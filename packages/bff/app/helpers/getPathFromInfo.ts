import { GraphQLResolveInfo } from 'graphql';
export type PathFromInfo = (string | number)[]
export type GetPathFromInfo = (info?: GraphQLResolveInfo) => PathFromInfo | false;

export const getPathFromInfo: GetPathFromInfo = (info) => {
  if (!info || !info.path) {
    return false;
  }
  const res: PathFromInfo = [];
  let curPath = info.path;
  while (curPath) {
    if (curPath.key) {
      res.unshift(curPath.key);
      if (!!curPath.prev) {
        curPath = curPath.prev;
      } else break;
    } else break;
  }
  return res
};
